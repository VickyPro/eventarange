package com.eventarange.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eventarange.service.UserService;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {
	
	@Autowired
	private UserService userservice;
	
    @GetMapping("/employees/{id}")
    public ResponseEntity getEmployeeById(@PathVariable(value = "id") Long employeeId){
        return ResponseEntity.ok().body(userservice.getUserData(employeeId));
    }

}
